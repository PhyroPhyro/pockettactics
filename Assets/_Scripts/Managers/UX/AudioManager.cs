﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class AudioManager : MonoBehaviour
{
	static AudioManager instance;
	[SerializeField]private AudioSource _bgmSource;
	[SerializeField]private AudioSource _sfxSource;
	private List<AudioSource> extraSources = new List<AudioSource>();

	/// <summary>
	/// Set the panel sounds here, in this order: Neutral, Success, Failure.
	/// </summary>
	[SerializeField]private AudioClip[] panelClips;
	private AudioListener myListener;

	private static bool _Music;
	public static bool Music
	{
		get { return _Music; }
		set 
		{
			instance._bgmSource.mute = !value;
			_Music = value;
		}
	}

	private static bool _SoundFX;
	public static bool SoundFX
	{
		get { return _SoundFX; }
		set
		{
			instance._sfxSource.mute = !value;
			instance.extraSources.ForEach (aS => aS.mute = !value);
			_SoundFX = value;
		}
	}

	private void Awake ()
	{
		instance = this;
		myListener = GetComponent<AudioListener> ();
		DontDestroyOnLoad (gameObject);
    }
    private void OnEnable()
    {
        SceneManager.sceneLoaded += onLevelLoaded;
    }
    private void OnDisable()
    {
        SceneManager.sceneLoaded -= onLevelLoaded;
    }
    private void Start ()
	{
		Music = PlayerPrefs.GetInt ("Music", 1) == 1;
		SoundFX = PlayerPrefs.GetInt ("SoundFX", 1) == 1;
	}
	public void OnDestroy ()
	{
        PlayerPrefs.SetInt ("Music", Music ? 1 : 0);
        PlayerPrefs.SetInt ("SoundFX", SoundFX ? 1 : 0);
	}
	public void onLevelLoaded (Scene scene, LoadSceneMode mode)
	{
		var listeners = FindObjectsOfType<AudioListener> ();

		for (int i = 0; i < listeners.Length; i++)
		{
			if (listeners[i].Equals (myListener))
				continue;
			else
				Destroy (listeners [i]);
		}

		extraSources.ForEach (Destroy);
		extraSources.Clear ();
	}

	public static void ToggleMusic ()
	{
		Music = !Music;
	}
	public static void ToggleSoundFX ()
	{
		SoundFX = !SoundFX;
	}

	public static void PlayMusic (AudioClip musicClip, float delay = 0f)
	{
		instance._bgmSource.Stop ();
		instance._bgmSource.PlayDelayed (delay);
		instance._bgmSource.clip = musicClip;
		instance._bgmSource.Play ();
	}

	#region Simple SFX
	public static void PlaySound (AudioClip sfxClip)
	{
		if(instance._sfxSource.isPlaying && instance.extraSources.Count < 4)
		{
			var tmpSource = instance.gameObject.AddComponent<AudioSource> ();
			tmpSource.clip = sfxClip;
			tmpSource.loop = false;
			tmpSource.mute = !SoundFX;
			tmpSource.Play ();
			instance.extraSources.Add (tmpSource);
			instance.StartCoroutine (instance.RemoveExtraSource (tmpSource));
		}
		else
		{
			instance._sfxSource.PlayOneShot (sfxClip);
		}
	}
	
	public static void PlayGenericSound (GenericSounds sound)
	{
		PlaySound (instance.panelClips [(int)sound]);
	}
	#endregion

	#region LoopingSounds
	public static AudioSource PlayLoopingSound (AudioClip sound, float volume = 1, float delay = 0f)
	{
		var tmpSource = instance.gameObject.AddComponent<AudioSource> ();
		
		tmpSource.clip = sound;
		tmpSource.loop = true;
		tmpSource.volume = volume;
		tmpSource.mute = !SoundFX;
		tmpSource.PlayDelayed(delay);
		tmpSource.rolloffMode = AudioRolloffMode.Linear;
		
		tmpSource.Play ();

		instance.extraSources.Add (tmpSource);
		return tmpSource;
	}
	public static void StopLoopingSound (AudioSource source)
	{
		Destroy (source);
	}
	#endregion

	#region AudioSource Management
	private IEnumerator RemoveExtraSource (AudioSource source)
	{
		while (source.isPlaying)
		{
			yield return null;

			if (source == null)
				yield break;
		}
		
		instance.extraSources.Remove (source);
		Destroy (source);
	}

	public static AudioSource CreateNewSource ()
	{
		var audioSource = instance.gameObject.AddComponent<AudioSource> ();
		audioSource.mute = !_SoundFX;
		instance.extraSources.Add (audioSource);

		return audioSource;
	}
	#endregion
}
public enum GenericSounds
{
	Neutral,
	Progress,
	Regress
}