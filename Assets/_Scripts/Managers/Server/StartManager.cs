﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using SocketIO;
using System;
using System.Collections.Generic;

public class StartManager : MonoBehaviour {

    IEnumerator Start()
    {
        DontDestroyOnLoad(gameObject);
        yield return new WaitForSeconds(1f);

        ServerManager.RequestMotd(ShowMOTD);
    }

    private void ShowMOTD(Dictionary<string, string> objDict)
    {
        String message = objDict["message"].Replace("\\n", "\n").Replace("\\r","");
        PopupManager.instance.SetPopup(PopupManager.PopupType.MOTD, "Message of the Day", message, null, MOTDCallback);
    }

    private void MOTDCallback()
    {
        SignInManager.instance.StartGoogleSignIn();
    }
}
