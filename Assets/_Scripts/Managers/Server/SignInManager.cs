﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class SignInManager : MonoBehaviour {

    public static SignInManager instance;
    private string currentUserToken = "";

    void Start () {
        instance = this;
	}

    public void StartGoogleSignIn()
    {
        GooglePlayConnection.ActionConnectionResultReceived += ActionConnectionResultReceived;
        GooglePlayConnection.Instance.Connect();

        GoogleCloudMessageService.ActionCMDRegistrationResult += RegistrationHandler;
        GoogleCloudMessageService.ActionGCMPushReceived += PushHandler;

        //Debug
        #if UNITY_EDITOR
        //ServerManager.VerifyGoogleAccount("123456789", GoogleSignInCallback);
        ServerManager.VerifyGoogleAccount("987654321", GoogleSignInCallback);
        #endif
    }

    private void RegistrationHandler(GP_GCM_RegistrationResult result)
    {
        if (result.IsSucceeded)
        {
            Debug.Log("SUCCESS ON REGISTER DEVICE");
            ServerManager.SubmitRegistration(GoogleCloudMessageService.Instance.registrationId, PlayerManager.instance.CurrentPlayer.token);
        }
        else
        {
            Debug.Log("ERROR ON REGISTER DEVICE");
        }
    }

    private void PushHandler(string message, Dictionary<string, object> dict)
    {
        Debug.Log(message);
        Debug.Log(dict.ToString());
    }

    private void ActionConnectionResultReceived(GooglePlayConnectionResult result)
    {
        if (result.IsSuccess)
        {
            Debug.Log("Connected!");

            ServerManager.VerifyGoogleAccount(GooglePlayManager.Instance.player.playerId, GoogleSignInCallback);
        }
        else
        {
            Debug.Log("Cnnection failed with code: " + result.code.ToString());
            Debug.Log("Trying again...");
            GooglePlayConnection.Instance.Connect();
        }
    }

    private void GoogleSignInCallback(Dictionary<string, string> objDict)
    {
        bool HasAccount = objDict.ContainsKey("name");
        Debug.Log("Has account: " + HasAccount);

        currentUserToken = objDict["token"];

        //Debug
        if (HasAccount)
        {
            StoreCurrentPlayer(objDict);
        }
        else
        {
            FindObjectOfType<SignInViewController>().ShowNicknameInput();
        }
    }

    public void SubmitAccountNickname(string nickname)
    {
        ServerManager.SubmitAccountNick(nickname, currentUserToken, StoreCurrentPlayer);
    }

    private void StoreCurrentPlayer(Dictionary<string,string> objDict)
    {
        GoogleCloudMessageService.Instance.Init();
        GoogleCloudMessageService.Instance.RgisterDevice();

        Player newPlayer = StaticMethods.PlayerFromDB(objDict);
        PlayerManager.instance.SetPlayer(newPlayer);

        SceneManager.LoadScene("MainMenu");
    }
}
