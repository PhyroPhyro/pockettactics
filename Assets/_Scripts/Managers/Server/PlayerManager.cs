﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SocketIO;

public class PlayerManager : MonoBehaviour {

    public static PlayerManager instance;

    public Player CurrentPlayer;

    void Start()
    {
        instance = this;
        SocketIOComponent socket = FindObjectOfType<SocketIOComponent>();
        socket.On("UpdatePlayerInfo", SyncPlayerInfo);
        socket.On("UpdatePlayerRanks", SyncPlayerRanks);
        socket.On("UpdatePlayerArmory", SyncPlayerArmory);
        socket.On("UpdatePlayerInventory", SyncPlayerInventory);
    }

    public void SetPlayer(Player _player)
    {
        CurrentPlayer = _player;
        RefreshPlayerArmory();
        RefreshPlayerInventory();
    }

    public void RefreshPlayerArmory()
    {
        CurrentPlayer.playerArmory = new List<TroopData>();
        ServerManager.GetMyArmory();
    }

    public void RefreshPlayerInventory()
    {
        CurrentPlayer.playerInventory = new List<LootData>();
        ServerManager.GetMyInventory();
    }

    private void SyncPlayerArmory(SocketIOEvent e)
    {
        Dictionary<string, string> objDict = e.data.ToDictionary();
        CurrentPlayer.playerArmory.Add(StaticMethods.TroopDataFromDBDict(objDict));
        EventManager.PlayerInfoUpdate();
    }

    private void SyncPlayerInventory(SocketIOEvent e)
    {
        Dictionary<string, string> objDict = e.data.ToDictionary();
        CurrentPlayer.playerInventory.Add(StaticMethods.LootDataFromDict(objDict));
        EventManager.PlayerInfoUpdate();
    }

    private void SyncPlayerInfo(SocketIOEvent e)
    {
        Dictionary<string, string> objDict = e.data.ToDictionary();
        CurrentPlayer = StaticMethods.PlayerFromDB(objDict);
        EventManager.PlayerInfoUpdate();
    }

    private void SyncPlayerRanks(SocketIOEvent e)
    {
        Dictionary<string, string> objDict = e.data.ToDictionary();
        CurrentPlayer.globalRank = objDict["_currentGlobalRank"];
        CurrentPlayer.honorRank = objDict["_currentHonorRank"];
        CurrentPlayer.globalRankExp = int.Parse(objDict["_globalRankExp"]);
        CurrentPlayer.honorRankExp = int.Parse(objDict["_honorRankExp"]);
        CurrentPlayer.nextGlobalRank = int.Parse(objDict["_nextGlobalRankValue"]);
        CurrentPlayer.nextHonorRank = int.Parse(objDict["_nextHonorRankValue"]);
        EventManager.PlayerInfoUpdate();
    }
}
