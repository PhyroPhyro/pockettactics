﻿using UnityEngine;
using System.Collections;
using SocketIO;
using System.Collections.Generic;

public class ServerManager : MonoBehaviour
{
    public static ServerManager instance;

    public string clientID;
    public static SocketIOComponent socket;
    private static Callback CurrentCallback;
    public delegate void Callback(Dictionary<string,string> objDict);

    private bool HasError = false;

    private static string RequestMOTD = "requestMOTD";

    private static string VerifyGoogle = "verifyGoogleAccount";
    private static string SubmitNickname = "submitAccountNickname";
    private static string SubmitRegistrationID = "submitRegistrationID";

    private static string GetMatch = "requestMatch";
    private static string RequestMyMatches = "requestAllMatches";

    private static string RequestMyArmory = "requestMyArmory";
    private static string RequestArmoryTroops = "requestArmoryTroops";
    private static string RequestArmorySave = "requestArmorySave";
    private static string RequestBuyTroop = "requestBuyTroop";

    private static string RequestMyInventory = "requestMyInventory";
    private static string RequestMyRanks = "requestMyRanks";

    private static string OpenLootBox = "openLootBox";

    private static string RequestGameSetup = "gameSetup";
    private static string RequestMap = "requestMap";
    private static string RequestTroops = "requestTroops";
    private static string RequestMatchArmory = "requestMatchArmory";
    private static string RequestEndTurn = "requestEndTurn";

    private static string VerifyCellPath = "verifyCellPath";
    private static string VerifyCellAttack = "verifyAttackPath";
    private static string VerifySummonPath = "verifySummonPath";

    #region Init
    void Start()
    {
        instance = this;
        socket = GetComponent<SocketIOComponent>();

        //Debug
        socket.On("open", TestOpen);
        socket.On("error", TestError);
        socket.On("close", TestClose);

        socket.On("SetClientID", SetClientID);
        socket.On("DefaultCallback", ReceiveEmit);
    }

    private void SetClientID(SocketIOEvent e)
    {
        Dictionary<string, string> objDict = e.data.ToDictionary();
        clientID = objDict["message"];
    }

    public static void RequestMotd(Callback callback)
    {
        CurrentCallback = callback;
        JSONObject obj = new JSONObject();

        Debug.Log("Requesting MOTD");
        SendEmit(RequestMOTD, obj);
    }
    #endregion

    #region Send/Receive
    private static void SendEmit(string emitName, JSONObject obj)
    {
        if(PlayerManager.instance.CurrentPlayer != null)
            obj.AddField("token", PlayerManager.instance.CurrentPlayer.token);

        obj.AddField("clientID", ServerManager.instance.clientID);
        socket.Emit(emitName, obj);
    }

    public void ReceiveEmit(SocketIOEvent e)
    {
        Debug.Log("Received: " + e.data);
        CurrentCallback(e.data.ToDictionary());
    }
    #endregion

    #region Account
    public static void VerifyGoogleAccount(string id, Callback callback)
    {
        CurrentCallback = callback;
        JSONObject obj = new JSONObject();
        obj.AddField("id", id);

        Debug.Log("Sending Verifying Google Account");
        SendEmit(VerifyGoogle, obj);
    }

    public static void SubmitAccountNick(string name, string token, Callback callback)
    {
        CurrentCallback = callback;
        JSONObject obj = new JSONObject();
        obj.AddField("name", name);
        obj.AddField("token", token);

        Debug.Log("Submiting Account Nick: " + name);
        SendEmit(SubmitNickname, obj);
    }

    public static void SubmitRegistration(string regID, string token)
    {
        JSONObject obj = new JSONObject();
        obj.AddField("regID", regID);
        obj.AddField("token", token);

        Debug.Log("Submiting Registration ID: " + regID);
        SendEmit(SubmitRegistrationID, obj);
    }
    #endregion

    #region Ranks
    public static void GetMyRanks()
    {
        JSONObject obj = new JSONObject();

        Debug.Log("Getting my ranks");
        SendEmit(RequestMyRanks, obj);
    }
    #endregion

    #region Armory
    public static void GetMyArmory()
    {
        JSONObject obj = new JSONObject();

        Debug.Log("Getting my armory");
        SendEmit(RequestMyArmory, obj);
    }

    public static void GetArmoryTroops()
    {
        JSONObject obj = new JSONObject();

        Debug.Log("Getting my armory");
        SendEmit(RequestArmoryTroops, obj);
    }

    public static void SavePlayerArmory(int leader, int tank, int melee, int ranged, int flying, int building, Callback callback)
    {
        CurrentCallback = callback;
        JSONObject obj = new JSONObject();
        obj.AddField("leader", leader);
        obj.AddField("tank", tank);
        obj.AddField("melee", melee);
        obj.AddField("ranged", ranged);
        obj.AddField("flying", flying);
        obj.AddField("building", building);

        Debug.Log("Saving my armory");
        SendEmit(RequestArmorySave, obj);
    }

    public static void BuyTroop(int troopID, Callback callback)
    {
        CurrentCallback = callback;
        JSONObject obj = new JSONObject();
        obj.AddField("troopID", troopID);

        Debug.Log("Buying troop " + troopID);
        SendEmit(RequestBuyTroop, obj);
    }
    #endregion

    #region Inventory
    public static void GetMyInventory()
    {
        JSONObject obj = new JSONObject();

        Debug.Log("Getting my inventory");
        SendEmit(RequestMyInventory, obj);
    }
    #endregion

    #region Loot
    public static void OpenServerLootBox(Callback callback)
    {
        CurrentCallback = callback;
        JSONObject obj = new JSONObject();

        Debug.Log("Opening loot box");
        SendEmit(OpenLootBox, obj);
    }
    #endregion

    #region Match
    public static void RequestNewMatch(int mapID, Callback callback)
    {
        CurrentCallback = callback;
        JSONObject obj = new JSONObject();
        obj.AddField("mapID", mapID);

        Debug.Log("Creating new Match");
        SendEmit(GetMatch, obj);
    }
    public static void GetMyMatches()
    {
        JSONObject obj = new JSONObject();

        Debug.Log("Getting my matches");
        SendEmit(RequestMyMatches, obj);
    }
    #endregion

    #region Game
    public static void GetGameSetup(int matchID, string token, int mapID, Callback callback)
    {
        CurrentCallback = callback;
        JSONObject obj = new JSONObject();
        obj.AddField("mapID", mapID);
        obj.AddField("token", token);
        obj.AddField("matchID", matchID);

        Debug.Log("Requesting game setup from server");
        SendEmit(RequestGameSetup, obj);
    }

    public static void GetMapFromServer()
    {
        JSONObject obj = new JSONObject();

        Debug.Log("Requesting map from server");
        SendEmit(RequestMap, obj);
    }

    public static void GetTroopsFromServer()
    {
        JSONObject obj = new JSONObject();

        Debug.Log("Requesting troops from server");
        SendEmit(RequestTroops, obj);
    }

    public static void VerifyTroopMovement(List<int> pathCellsID, int fromCellID, int troopUID, Callback callback)
    {
        CurrentCallback = callback;

        JSONObject obj = new JSONObject();
        for (int i = 1; i <= pathCellsID.Count; i++)
        {
            obj.AddField("cell" + i.ToString(), pathCellsID[i - 1]);
        }
        obj.AddField("fromCellID", fromCellID);
        obj.AddField("troopUID", troopUID);

        Debug.Log("Sending path to verification");
        SendEmit(VerifyCellPath, obj);
    }

    public static void VerifyTroopAttack(int attackCellID, List<int> distanceCellsID, int attackFromCellID, Callback callback)
    {
        CurrentCallback = callback;
        JSONObject obj = new JSONObject();

        for (int i = 1; i <= distanceCellsID.Count; i++)
        {
            obj.AddField("distanceCell" + i.ToString(), distanceCellsID[i - 1]);
        }
        obj.AddField("attackFromID", attackFromCellID);
        obj.AddField("attackToID", attackCellID);

        Debug.Log("Sending attack to verification");
        SendEmit(VerifyCellAttack, obj);
    }

    public static void VerifyTroopSummon(int troopArmoryID, int summonCellID, Callback callback)
    {
        CurrentCallback = callback;
        JSONObject obj = new JSONObject();

        obj.AddField("troopArmoryID", troopArmoryID);
        obj.AddField("summonCellID", summonCellID);

        Debug.Log("Sending summon to verification");
        SendEmit(VerifySummonPath, obj);
    }

    public static void ServerEndTurn(Callback callback)
    {
        CurrentCallback = callback;
        JSONObject obj = new JSONObject();

        Debug.Log("Sending request to end turn");
        SendEmit(RequestEndTurn, obj);
    }
    #endregion

    #region Debug
    public void TestOpen(SocketIOEvent e)
    {
        Debug.Log("[SocketIO] Open received: " + e.name + " " + e.data);
    }

    public void TestError(SocketIOEvent e)
    {
        Debug.Log("[SocketIO] Error received: " + e.name + " " + e.data);
        HasError = true;
        //socket.Close();
    }

    public void TestClose(SocketIOEvent e)
    {
        Debug.Log("[SocketIO] Close received: " + e.name + " " + e.data);
        HasError = true;
    }

    void Update()
    {
        if(HasError)
            PopupManager.instance.SetPopup(PopupManager.PopupType.InfoPopup, "Error", "There was some error connecting with server. Please, try again.", null, QuitApp);
    }

    private void QuitApp()
    {
        Application.Quit();
    }
    #endregion
}
