﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class LootViewManager : MonoBehaviour {

    public Text txCurrentBoxes;
    public Button btOpenBox;

    void OnEnable()
    {
        EventManager.OnPlayerInfoUpdate += LootViewSetup;
    }

    void OnDisable()
    {
        EventManager.OnPlayerInfoUpdate -= LootViewSetup;
    }

    private void LootViewSetup()
    {
        int boxCount = 0;
        List<LootData> playerInv = new List<LootData>();
        playerInv = PlayerManager.instance.CurrentPlayer.playerInventory;

        for (int i = 0; i < playerInv.Count; i++)
        {
            if (playerInv[i].lootType == LootData.LootTypes.Box)
                boxCount = playerInv[i].lootAmount;
        }

        if (boxCount <= 0)
            btOpenBox.gameObject.SetActive(false);
        else
            btOpenBox.gameObject.SetActive(true);

        txCurrentBoxes.text = string.Format("You have {0} boxes to open", boxCount);
    }

    public void OpenBox()
    {
        ServerManager.OpenServerLootBox(OpenBoxCallback);
    }

    private void OpenBoxCallback(Dictionary<string,string> objDict)
    {
        PlayerManager.instance.RefreshPlayerInventory();
        PopupManager.instance.SetPopup(PopupManager.PopupType.InfoPopup, "Congratulations", string.Format("You've got\n{0}", objDict["name"]), null, LootViewSetup);
    }
}