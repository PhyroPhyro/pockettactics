﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PopupManager : MonoBehaviour {

    public static PopupManager instance;
    public List<GameObject> popupList = new List<GameObject>();
    public delegate void Callback();
    public GameObject Blackground;
    private Callback PopupCallback;

    public enum PopupType
    {
        InfoPopup = 0,
        ConfirmationPopup = 1,
        MOTD = 2
    }

    void Start () {
        instance = this;
        DontDestroyOnLoad(gameObject);
	}

    public void SetPopup(PopupType popupType, string _titleText, string _bodyText, string _subText = null, Callback _callback = null)
    {
        CloseAllPopups();

        GameObject currPopup = popupList[(int)popupType];

        currPopup.GetComponent<PopupController>().titleText.text = _titleText;
        currPopup.GetComponent<PopupController>().bodyText.text = _bodyText;
        if (currPopup.GetComponent<PopupController>().subText)
            currPopup.GetComponent<PopupController>().subText.text = _subText;

        PopupCallback = _callback;
        currPopup.SetActive(true);
        Blackground.SetActive(true);
    }

    public void OnConfirmation()
    {
        CloseAllPopups();
        if (PopupCallback != null)
            PopupCallback();
    }
    private void CloseAllPopups()
    {
        for (int i = 0; i < popupList.Count; i++)
        {
            popupList[i].SetActive(false);
        }
        Blackground.SetActive(false);
    }
}
