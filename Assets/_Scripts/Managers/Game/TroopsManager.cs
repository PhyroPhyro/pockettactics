﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SocketIO;

public class TroopsManager : MonoBehaviour {

    public static TroopsManager instance;

    public enum TroopsTypes
    {
        Fire = 1,
        Water = 2,
        Grass = 3,
        Dark = 4,
        Light = 5
    }

    public enum TroopsClasses
    {
        Leader = 1,
        Tank = 2,
        Melee = 3,
        Ranged = 4,
        Flying = 5,
        Building = 6
    }

    public enum TroopStates
    {
        Idle = 0,
        Moved = 1,
        Attacked = 2
    }

    public enum TroopAbilities
    {
        None = 0,
        Hastes = 1,
        Block = 2,
        DoubleStrike = 3,
        Spawner = 4
    }

    private bool IsTroopsLoaded = false;

    //Player hero troops that they entered the match
    /*public Troop Player1Hero;
    public Troop Player2Hero;*/

    //Troops list from database and respective objects/models
    public List<Troop> TroopList = new List<Troop>();
    public List<GameObject> TroopGameObjects = new List<GameObject>();
    public Troop HeroTroop;
    public GameObject troopContainer;

    //Troops waiting for commands from server
    public Troop troopToMove, attackingTroop, defendingTroop;
    public List<Cell> pathToMove;

    void Start()
    {
        FindObjectOfType<SocketIOComponent>().On("UpdateClientTroops", SyncTroops);
        instance = this;
    }

    public IEnumerator RequestTroops()
    {
        ServerManager.GetTroopsFromServer();

        while (!IsTroopsLoaded)
            yield return null;
    }

    private void SyncTroops(SocketIOEvent e)
    {
        Dictionary<string, string> objDict = e.data.ToDictionary();

        if (objDict.ContainsKey("message"))
        {
            if (objDict["message"] == "done")
            {
                IsTroopsLoaded = true;
                GetCurrentHero();
                Debug.Log("Troops done loading");
            }
            else
            {
                PopupManager.instance.SetPopup(PopupManager.PopupType.InfoPopup, "Oops", objDict["message"]);
            }
        }

        Debug.Log("Receiving troop: " + objDict["_id"] + " UID " + objDict["_uid"]);

        TroopData currTroop = StaticMethods.TroopDataFromDict(objDict);

        UpdateTroopOnMap(currTroop);
    }

    private void UpdateTroopOnMap(TroopData currTroopData)
    {
        for (int i = 0; i < TroopList.Count; i++)
        {
            if (TroopList[i].UID == currTroopData.UID)
            {
                TroopList[i].FromTroopData(currTroopData);
                return;
            }
        }

        CreateTroop(currTroopData);
    }

    public void CreateTroop(TroopData currTroop)
    {
        GameObject troopObj = GameObject.Instantiate(TroopGameObjects[currTroop.ID - 1]) as GameObject;
        troopObj.name = "Troop" + currTroop.ID;
        troopObj.transform.SetParent(troopContainer.transform);
        troopObj.GetComponent<Troop>().FromTroopData(currTroop);

        TroopList.Add(troopObj.GetComponent<Troop>());

        MapManager.instance.SnapTroopsToCells();
    }

    private void GetCurrentHero()
    {
        for (int i = 0; i < TroopList.Count; i++)
        {
            if (TroopList[i].IsMine() && TroopList[i].TroopClass == TroopsClasses.Leader)
                HeroTroop = TroopList[i];
        }
    }

    public Troop GetCellTroopByUID(int uid)
    {
        for (int i = 0; i < TroopList.Count; i++)
        {
            if (TroopList[i].UID == uid)
                return TroopList[i];
        }
        return null;
    }

    public void MoveTroop(Cell fromCell, Cell toCell)
    {
        EventManager.TroopStarted();

        Troop CurrentTroop = fromCell.CellTroop();

        MapManager.instance.UnselectAllCells();
        MapManager.instance.ShowCellPath(fromCell, toCell);

        troopToMove = CurrentTroop;
        pathToMove = MapManager.instance.FindPathCells(fromCell, toCell);
        List<int> pathIntList = new List<int>();
        for (int i = 0; i < pathToMove.Count; i++)
        {
            pathIntList.Add(pathToMove[i].ID);
        }
        ServerManager.VerifyTroopMovement(pathIntList, fromCell.ID, CurrentTroop.UID, WaitForMovementVerification);
    }

    private void WaitForMovementVerification(Dictionary<string,string> objDict)
    {
        if (objDict.ContainsKey("message"))
        {
            if (objDict["message"] == "ok")
            {
                StartCoroutine(DoMoveTroop());
            }
            else
            {
                Debug.Log(objDict["message"]);
                PopupManager.instance.SetPopup(PopupManager.PopupType.InfoPopup, "Error", objDict["message"]);
            }
        }
    }

    IEnumerator DoMoveTroop()
    {
        //Begin troop move
        yield return StartCoroutine(troopToMove.MoveThisTroop(pathToMove));
        //Troop move ended
    }

    public void AttackTroop(Cell fromCell, Cell onCell)
    {
        MapManager.instance.UnselectAllCells();

        List<Cell> atkDistancePath = MapManager.instance.FindAttackPathCells(fromCell, onCell);
        List<int> distanceIDs = new List<int>();
        for (int i = 0; i < atkDistancePath.Count; i++)
        {
            distanceIDs.Add(atkDistancePath[i].ID);
        }

        attackingTroop = fromCell.CellTroop();
        defendingTroop = onCell.CellTroop();
        ServerManager.VerifyTroopAttack(onCell.ID, distanceIDs, fromCell.ID, WaitForAttackVerification);
    }

    private void WaitForAttackVerification(Dictionary<string, string> objDict)
    {
        if (objDict.ContainsKey("message"))
        {
            if (objDict["message"] == "ok")
            {
                StartCoroutine(DoAttackTroop());
            }
            else
            {
                Debug.Log(objDict["message"]);
                PopupManager.instance.SetPopup(PopupManager.PopupType.InfoPopup, "Error", objDict["message"]);
            }
        }
    }

    IEnumerator DoAttackTroop()
    {
        attackingTroop.AttackingEvent();
        defendingTroop.DefenseEvent();
        yield return new WaitForSeconds(2f);
        defendingTroop.DefenseEvent();
        attackingTroop.AttackingEvent();
        yield return new WaitForSeconds(2f);
        if (attackingTroop.Life <= 0)
            attackingTroop.DieEvent();
        if (defendingTroop.Life <= 0)
            defendingTroop.DieEvent();

        EventManager.AttackFinished();
    }

    public void SummonTroop(Cell summonCell)
    {
        MapManager.instance.UnselectAllCells();
        ServerManager.VerifyTroopSummon(ArmoryManager.instance.selectedTroopArmory.ID, summonCell.ID, WaitForSummonVerification);
    }

    private void WaitForSummonVerification(Dictionary<string, string> objDict)
    {
        if (objDict.ContainsKey("message"))
        {
            if (objDict["message"] == "ok")
            {
                StartCoroutine(DoSummonTroop());
            }
            else
            {
                Debug.Log(objDict["message"]);
                PopupManager.instance.SetPopup(PopupManager.PopupType.InfoPopup, "Error", objDict["message"]);
            }
        }
    }

    IEnumerator DoSummonTroop()
    {
        //TODO - Energy management after summon
        yield return null;
    }

    private void CheckVictory()
    {
        /*if (Player1Hero == null)
            Debug.Log("PLAYER 2 WINS");
        else if(Player2Hero == null)
            Debug.Log("PALYER 1 WINS");*/
    }
}