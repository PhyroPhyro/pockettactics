﻿using UnityEngine;
using System.Collections;
using SocketIO;
using System.Collections.Generic;

public class ArmoryManager : MonoBehaviour {

    public static ArmoryManager instance;
    public GameObject armoryContainer, armoryObj;
    public TroopData selectedTroopArmory;
    private List<TroopArmory> armoryTroops = new List<TroopArmory>();

	void Start()
    {
        instance = this;
        FindObjectOfType<SocketIOComponent>().On("UpdateClientArmory", RefreshArmory);
    }

    private void RefreshArmory(SocketIOEvent e)
    {
        Dictionary<string, string> objDict = e.data.ToDictionary();
        TroopData newTroopToArmory = StaticMethods.TroopDataFromDict(objDict);

        Debug.Log("Receiving armory troop id " + newTroopToArmory.ID.ToString());

        for (int i = 0; i < armoryTroops.Count; i++)
        {
            if (armoryTroops[i].troopData.ID == newTroopToArmory.ID)
            {
                armoryTroops[i].Refresh(newTroopToArmory);
                return;
            }
        }

        GameObject currArmoryObj = Instantiate(armoryObj) as GameObject;
        currArmoryObj.transform.SetParent(armoryContainer.transform);
        currArmoryObj.transform.localScale = Vector3.one;
        currArmoryObj.GetComponent<TroopArmory>().Refresh(newTroopToArmory);
        armoryTroops.Add(currArmoryObj.GetComponent<TroopArmory>());
    }

    public void SelectArmoryTroop(TroopData _troop)
    {
        if(GameManager.instance.PlayerEnergy >= _troop.Cost)
        {
            selectedTroopArmory = _troop;
            MapManager.instance.SelectSummonCells();
        }
    }
}
