﻿using UnityEngine;
using System.Collections;

public class EventManager : MonoBehaviour
{

    public delegate void EventActions();

    #region GAME
    public static event EventActions OnGameStarted;
    public static void GameStarted()
    {
        if (OnGameStarted != null)
            OnGameStarted();
    }

    public static event EventActions OnGameFinished;
    public static void GameFinished()
    {
        if (OnGameFinished != null)
            OnGameFinished();
    }

    public static event EventActions OnTroopStarted;
    public static void TroopStarted()
    {
        if (OnTroopStarted != null)
            OnTroopStarted();
    }

    public static event EventActions OnTroopFinished;
    public static void TroopFinished()
    {
        if (OnTroopFinished != null)
            OnTroopFinished();
    }

    public static event EventActions OnTroopDefending;
    public static void TroopDefending()
    {
        if (OnTroopDefending != null)
            OnTroopDefending();
    }

    public static event EventActions OnTroopAttacking;
    public static void TroopAttacking()
    {
        if (OnTroopAttacking != null)
            OnTroopAttacking();
    }

    public static event EventActions OnTroopSkill;
    public static void TroopSkill()
    {
        if (OnTroopSkill != null)
            OnTroopSkill();
    }

    public static event EventActions OnTroopDie;
    public static void TroopDie()
    {
        if (OnTroopDie != null)
            OnTroopDie();
    }

    public static event EventActions OnAttackFinished;
    public static void AttackFinished()
    {
        if (OnAttackFinished != null)
            OnAttackFinished();
    }
    #endregion

    #region UI
    public static event EventActions OnPlayerInfoUpdate;
    public static void PlayerInfoUpdate()
    {
        if (OnPlayerInfoUpdate != null)
            OnPlayerInfoUpdate();
    }
    #endregion
}
