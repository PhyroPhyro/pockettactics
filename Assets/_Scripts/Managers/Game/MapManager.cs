﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using SocketIO;

public class MapManager : MonoBehaviour {

    public static MapManager instance;
    public enum CellType
    {
        Empty = -1,
        Normal = 0,
        HeroSpawn = 1,
    }

    public GameObject spawnThisCell, cellContainer;
    public Vector3 finalRotation;

    private string[] MapLines;
    public float radius = 0.5f;
    public bool useAsInnerCircleRadius = true;
    private float offsetX, offsetY;

    private List<Cell> HeroSpawns = new List<Cell>();
    public Cell SelectedCell;

    private bool IsMapLoaded = false;
    public List<Cell> MapCells = new List<Cell>();

    void Start()
    {
        instance = this;

        FindObjectOfType<SocketIOComponent>().On("UpdateClientMap", SyncMap);
    }

    void OnEnable()
    {
        EventManager.OnTroopFinished += UnselectAllCells;
    }

    void OnDisable()
    {
        EventManager.OnTroopFinished -= UnselectAllCells;
    }

    public IEnumerator RequestMap()
    {
        ServerManager.GetMapFromServer();

        while (!IsMapLoaded)
            yield return null;
    }

    public void SyncMap(SocketIOEvent e)
    {
        Dictionary<string, string> objDict = e.data.ToDictionary();
        if (objDict.ContainsKey("message"))
        {
            if (objDict["message"] == "done")
            {
                cellContainer.transform.localEulerAngles = finalRotation;
                UpdateCellsNeighbors();
            }
        }

        Debug.Log("Receiving cell: " + objDict["_id"]);

        CellData currCell = new CellData();
        currCell.id = int.Parse(objDict["_id"]);
        currCell.cellTroopUID = int.Parse(objDict["_cellTroopUID"]);
        currCell.value = int.Parse(objDict["_value"]);
        currCell.xCoord = int.Parse(objDict["_xCoord"]);
        currCell.yCoord = int.Parse(objDict["_yCoord"]);

        UpdateCellOnMap(currCell);
    }

    private void UpdateCellOnMap(CellData cellData)
    {
        for (int i = 0; i < MapCells.Count; i++)
        {

            if (MapCells[i].ID == cellData.id)
                MapCells[i].FromCellData(cellData);
        }

        if (MapCells.Count > 0)
        {
            for (int i = 0; i < MapCells.Count; i++)
            {
                if(cellData.id >= MapCells.Count)
                {
                    CreateCell(cellData);
                }
                else
                {
                    Cell currCellObj = MapCells[i];
                    if (currCellObj.ID == cellData.id)
                        currCellObj.FromCellData(cellData);
                }
            }
        }else
        {
            CreateCell(cellData);
        }
    }

    public void CreateCell(CellData currCell)
    {
        float unitLength = (useAsInnerCircleRadius) ? (radius / (Mathf.Sqrt(3) / 2)) : radius;

        offsetX = unitLength * Mathf.Sqrt(3);
        offsetY = unitLength * 1.5f;

        int x = currCell.xCoord;
        int y = currCell.yCoord;

        Vector2 hexpos = HexOffset(x, y);
        Vector3 pos = new Vector3(hexpos.x, hexpos.y, 0);

        GameObject cellObj = GameObject.Instantiate(spawnThisCell) as GameObject;
        cellObj.transform.localPosition = pos;
        cellObj.transform.localRotation = Quaternion.identity;
        cellObj.name = "Cell" + currCell.id;
        cellObj.transform.SetParent(cellContainer.transform);
        cellObj.GetComponent<Cell>().FromCellData(currCell);

        MapCells.Add(cellObj.GetComponent<Cell>());
    }

    private void UpdateCellsNeighbors()
    {
        for (int i = 0; i < MapCells.Count; i++)
        {
            MapCells[i].SetNeighbours();
        }

        IsMapLoaded = true;
        Debug.Log("Map done loading");
    }

    public void SnapTroopsToCells()
    {
        List<Troop> troopList = TroopsManager.instance.TroopList;
        for (int i = 0; i < troopList.Count; i++)
        {
            for (int j = 0; j < MapCells.Count; j++)
            {
                if(MapCells[j].CellTroopUID == troopList[i].UID)
                {
                    troopList[i].transform.position = MapCells[j].PositionNode.position;
                }
            }
        }
    }

    Vector2 HexOffset(int x, int y)
    {
        Vector2 position = Vector2.zero;

        if (y % 2 == 0)
        {
            position.x = x * offsetX;
            position.y = y * offsetY;
        }
        else
        {
            position.x = (x + 0.5f) * offsetX;
            position.y = y * offsetY;
        }

        return position;
    }

    public List<Cell> FindPathCells(Cell fromCell, Cell toCell)
    {
        List<Cell> PathCells = new List<Cell>();
        Cell currCell = fromCell;
        while (currCell != toCell)
        {
            float lessDist = 99999f;
            Cell currNeigh = null;
            for (int i = 0; i < currCell.NeighboursCells.Count; i++)
            {
                float currNeighDist = (toCell.transform.position - currCell.NeighboursCells[i].transform.position).sqrMagnitude;
                if (currCell.NeighboursCells[i].CellTroopUID == 0 && !PathCells.Contains(currCell.NeighboursCells[i]))
                {
                    if (currNeighDist < lessDist)
                    {
                        lessDist = currNeighDist;
                        currNeigh = currCell.NeighboursCells[i];
                    }
                }
            }

            PathCells.Add(currNeigh);
            currCell = currNeigh;
        }

        return PathCells;
    }

    public List<Cell> FindAttackPathCells(Cell fromCell, Cell toCell)
    {
        List<Cell> PathCells = new List<Cell>();
        Cell currCell = fromCell;
        while (currCell != toCell)
        {
            float lessDist = 99999f;
            Cell currNeigh = null;
            for (int i = 0; i < currCell.NeighboursCells.Count; i++)
            {
                float currNeighDist = (toCell.transform.position - currCell.NeighboursCells[i].transform.position).sqrMagnitude;
                if (!PathCells.Contains(currCell.NeighboursCells[i]))
                {
                    if (currNeighDist < lessDist)
                    {
                        lessDist = currNeighDist;
                        currNeigh = currCell.NeighboursCells[i];
                    }
                }
            }

            PathCells.Add(currNeigh);
            currCell = currNeigh;
        }

        return PathCells;
    }

    public List<Cell> FindDistanceCells(Cell fromCell, int allowedDistance, bool includeEnemys)
    {
        int currDistance = allowedDistance - 1;
        List<List<Cell>> currNeighs = new List<List<Cell>>();
        currNeighs.Add(fromCell.NeighboursCells);
        while(currDistance > 0)
        {
            int count = currNeighs.Count;
            for (int i = 0; i < count; i++)
            {
                for (int j = 0; j < currNeighs[i].Count; j++)
                {
                    if(currNeighs[i][j].CellTroopUID == 0 && !currNeighs.Contains(currNeighs[i][j].NeighboursCells))
                        currNeighs.Add(currNeighs[i][j].NeighboursCells);
                }
            }

            currDistance--;
        }

        List<Cell> finalCells = new List<Cell>();
        for (int i = 0; i < currNeighs.Count; i++)
        {
            for (int j = 0; j < currNeighs[i].Count; j++)
            {
                if(currNeighs[i][j].CellTroopUID == 0|| includeEnemys)
                    finalCells.Add(currNeighs[i][j]);
            }
        }

        return finalCells;
    }

    public void SelectCellTroop(Cell cell)
    {
        UnselectAllCells();

        if (cell.CellTroop().TroopStatus == TroopsManager.TroopStates.Idle)
        {
            SelectTroopMoving(cell);
        }

        if (cell.CellTroop().TroopStatus == TroopsManager.TroopStates.Idle || cell.CellTroop().TroopStatus == TroopsManager.TroopStates.Moved)
        {
            SelectTroopAttacking(cell);
        }
    }

    public void SelectSummonCells()
    {
        Cell heroCell = null;
        for (int i = 0; i < MapCells.Count; i++)
        {
            if (MapCells[i].CellTroopUID == TroopsManager.instance.HeroTroop.UID)
                heroCell = MapCells[i];
        }
        List<Cell> PossibleSummonTargets = FindDistanceCells(heroCell, heroCell.CellTroop().SummonDistance, false);
        for (int i = 0; i < PossibleSummonTargets.Count; i++)
        {
            PossibleSummonTargets[i].SetCellSummonActive(true);
        }
    }

    public void SelectTroopMoving(Cell cell)
    {
        List<Cell> PossiblePaths = FindDistanceCells(cell, cell.CellTroop().Speed, false);
        for (int i = 0; i < PossiblePaths.Count; i++)
        {
            PossiblePaths[i].SetCellActive(true);
        }
        SelectedCell = cell;
    }

    public void SelectTroopAttacking(Cell cell)
    {
        List<Cell> PossibleTargets = FindDistanceCells(cell, cell.CellTroop().AtkDistance, true);
        for (int i = 0; i < PossibleTargets.Count; i++)
        {
            if (PossibleTargets[i].CellTroop() != null)
                if (!PossibleTargets[i].CellTroop().IsMine())
                    PossibleTargets[i].SetCellAttackActive(true);
        }
        SelectedCell = cell;
    }

    public void ShowCellPath(Cell fromCell, Cell toCell)
    {
        List<Cell> Path = FindPathCells(fromCell, toCell);
        for (int i = 0; i < Path.Count; i++)
        {
            Path[i].SetCellActive(true);
        }

        SelectedCell = null;
    }

    public void UnselectAllCells()
    {
        for (int i = 0; i < MapCells.Count; i++)
        {
            MapCells[i].SetCellActive(false);
            MapCells[i].SetCellAttackActive(false);
            MapCells[i].SetCellSummonActive(false);
        }

        SelectedCell = null;
    }

    public Cell GetCellFromUID(int troopUID)
    {
        for (int i = 0; i < MapCells.Count; i++)
        {
            if (MapCells[i].CellTroopUID == troopUID)
                return MapCells[i];
        }

        return null;
    }
}
