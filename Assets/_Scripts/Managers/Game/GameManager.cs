﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using SocketIO;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public static GameManager instance;

    public int PlayerEnergy;
    public bool CanMove = true;
    private bool SetupDone = false;

    void OnEnable()
    {
        EventManager.OnTroopStarted += BlockInput;
        EventManager.OnTroopAttacking += BlockInput;
        EventManager.OnTroopFinished += AllowInput;
        EventManager.OnAttackFinished += AllowInput;
    }

    void OnDisable()
    {
        EventManager.OnTroopStarted -= BlockInput;
        EventManager.OnTroopAttacking -= BlockInput;
        EventManager.OnAttackFinished -= AllowInput;
        EventManager.OnTroopFinished -= AllowInput;
    }

    IEnumerator Start()
    {
        FindObjectOfType<SocketIOComponent>().On("UpdateClientEnergy", SyncEnergy);
        FindObjectOfType<SocketIOComponent>().On("UpdateClientGameOver", SyncGameOver);

        instance = this;
        yield return null;
        yield return StartCoroutine(StartGame());
    }

    IEnumerator StartGame()
    {
        ServerManager.GetGameSetup(MatchManager.instance.CurrentMatch.id,
                                    PlayerManager.instance.CurrentPlayer.token,
                                    MatchManager.instance.ChosenMap, SetupGameCallback);
        while (!SetupDone)
            yield return null;

        yield return StartCoroutine(MapManager.instance.RequestMap());
        yield return StartCoroutine(TroopsManager.instance.RequestTroops());
    }

    private void SetupGameCallback(Dictionary<string,string> objDict)
    {
        if(objDict.ContainsKey("message"))
        {
            if (objDict["message"] == "done")
            {
                SetupDone = true;
            }
            else
            {
                Debug.Log(objDict["message"]);
                PopupManager.instance.SetPopup(PopupManager.PopupType.InfoPopup, "Error", objDict["message"], null, ErrorHandler);
            }
        }
    }

    private void ErrorHandler()
    {
        SceneManager.LoadScene("MainMenu");
    }

    private void SyncEnergy(SocketIOEvent e)
    {
        Dictionary<string, string> objDict = e.data.ToDictionary();
        PlayerEnergy = int.Parse(objDict["playerEnergy"]);
    }

    private void SyncGameOver(SocketIOEvent e)
    {
        Dictionary<string, string> objDict = e.data.ToDictionary();
        FindObjectOfType<GameOverViewController>().SetGameOver(objDict);
    }

    private void BlockInput()
    {
        CanMove = false;
    }

    private void AllowInput()
    {
        CanMove = true;
    }
}
