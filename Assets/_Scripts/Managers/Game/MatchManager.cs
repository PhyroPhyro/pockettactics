﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System;

public class MatchManager : MonoBehaviour {

    public static MatchManager instance;
    public Match CurrentMatch;
    public int ChosenMap;
    public enum MatchStatus
    {
        Empty = 0,
        Waiting = 1,
        Playing = 2,
        Completed = 3
    }


    void Start()
    {
        instance = this;
    }

    public static Match MatchConverter(Dictionary<string, string> matchDict)
    {
        Match newMatch = new Match();
        newMatch.id = int.Parse(matchDict["id"]);
        newMatch.player1ID = int.Parse(matchDict["player1ID"]);
        newMatch.status = (MatchStatus)int.Parse(matchDict["status"]);

        if(matchDict.ContainsKey("player2ID"))
            newMatch.player2ID = int.Parse(matchDict["player2ID"]);
        if (matchDict.ContainsKey("currentTurnID"))
            newMatch.turnID = int.Parse(matchDict["currentTurnID"]);
        if (matchDict.ContainsKey("lastPlayerID"))
            newMatch.lastPlayed = int.Parse(matchDict["lastPlayerID"]);
        if (matchDict.ContainsKey("opponentName"))
            newMatch.opponentName = matchDict["opponentName"];

        newMatch.lastUpdated = DateTime.Parse(matchDict["lastUpdate"], System.Globalization.CultureInfo.CurrentCulture);
        return newMatch;
    }

    public void StartMatch(Match match)
    {
        Debug.Log("Match started!");
        CurrentMatch = match;
        SceneManager.LoadScene("Game");
    }
}
