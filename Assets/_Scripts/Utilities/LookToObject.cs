﻿using UnityEngine;
using System.Collections;

public class LookToObject : MonoBehaviour {

    public Transform targetObject;

    void Start()
    {
        if (targetObject == null)
            targetObject = Camera.main.gameObject.transform;
    }

    void Update()
    {
        transform.LookAt(targetObject.transform);
    }
}
