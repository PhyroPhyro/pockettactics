﻿using System.Collections.Generic;

public static class StaticMethods {

    public static TroopData TroopDataFromDict(Dictionary<string, string> troopDict)
    {
        TroopData currTroop = new TroopData();
        //Strings
        currTroop.Name = troopDict["_name"];

        //Ints
        currTroop.ID = int.Parse(troopDict["_id"]);
        currTroop.UID = int.Parse(troopDict["_uid"]);
        currTroop.PlayerID = int.Parse(troopDict["_playerID"]);
        currTroop.Life = int.Parse(troopDict["_life"]);
        currTroop.MaxLife = int.Parse(troopDict["_maxLife"]);
        currTroop.Cost = int.Parse(troopDict["_cost"]);
        currTroop.TroopClass = int.Parse(troopDict["_troopClass"]);
        currTroop.Speed = int.Parse(troopDict["_speed"]);
        currTroop.AtkDistance = int.Parse(troopDict["_atkDistance"]);
        currTroop.AtkStr = int.Parse(troopDict["_atkStr"]);
        currTroop.TypeID = int.Parse(troopDict["_typeID"]);
        currTroop.TroopStatus = int.Parse(troopDict["_status"]);
        currTroop.TroopAbility = int.Parse(troopDict["_troopAbility"]);
        currTroop.TurnsToSpawn = int.Parse(troopDict["_turnsToSpawn"]);
        currTroop.MaxSpawnTurns = int.Parse(troopDict["_maxSpawnTurns"]);
        currTroop.SummonDistance = int.Parse(troopDict["_summonDistance"]);

        //Bools
        currTroop.CanAttack = int.Parse(troopDict["_canAttack"]) != 0;
        currTroop.CanMove = int.Parse(troopDict["_canMove"]) != 0;
        currTroop.CanFly = int.Parse(troopDict["_canFly"]) != 0;
        currTroop.CanAttackFlying = int.Parse(troopDict["_canAttackFlying"]) != 0;
        currTroop.IsProtected = int.Parse(troopDict["_isProtected"]) != 0;
        currTroop.HasDoubleAtk = int.Parse(troopDict["_hasDoubleAtk"]) != 0;

        return currTroop;
    }

    public static TroopData TroopDataFromDBDict(Dictionary<string, string> troopDict)
    {
        TroopData currTroop = new TroopData();
        //Strings
        currTroop.Name = troopDict["name"];

        //Ints
        currTroop.ID = int.Parse(troopDict["id"]);
        currTroop.Life = int.Parse(troopDict["life"]);
        currTroop.Cost = int.Parse(troopDict["troopCost"]);
        currTroop.BuyCost = int.Parse(troopDict["buyCost"]);
        currTroop.TroopClass = int.Parse(troopDict["troopClass"]);
        currTroop.Speed = int.Parse(troopDict["speed"]);
        currTroop.AtkDistance = int.Parse(troopDict["atkDistance"]);
        currTroop.AtkStr = int.Parse(troopDict["atkStr"]);
        currTroop.TypeID = int.Parse(troopDict["typeID"]);
        currTroop.SummonDistance = int.Parse(troopDict["summonDistance"]);
        currTroop.TroopAbility = int.Parse(troopDict["troopAbility"]);
        currTroop.TurnsToSpawn = int.Parse(troopDict["turnsToSpawn"]);

        //Bools
        currTroop.CanAttack = int.Parse(troopDict["canAtk"]) != 0;
        currTroop.CanMove = int.Parse(troopDict["canMove"]) != 0;
        currTroop.CanFly = int.Parse(troopDict["canFly"]) != 0;
        currTroop.CanAttackFlying = int.Parse(troopDict["canAtkFlying"]) != 0;
        return currTroop;
    }

    public static LootData LootDataFromDict(Dictionary<string, string> lootDict)
    {
        LootData currLoot = new LootData();
        //Strings
        currLoot.lootName = lootDict["_lootName"];

        //Ints
        currLoot.id = int.Parse(lootDict["_id"]);
        currLoot.lootID = int.Parse(lootDict["_lootID"]);
        currLoot.lootAmount = int.Parse(lootDict["_lootAmount"]);
        currLoot.playerID = int.Parse(lootDict["_playerID"]);

        //Enum
        currLoot.lootType = (LootData.LootTypes)int.Parse(lootDict["_lootType"]);

        return currLoot;
    }

    public static Player PlayerFromDB(Dictionary<string, string> objDict)
    {
        Player newPlayer = new Player();
        newPlayer.id = int.Parse(objDict["id"]);
        newPlayer.globalRankExp = int.Parse(objDict["globalRankExp"]);
        newPlayer.honorRankExp = int.Parse(objDict["honorRankExp"]);
        newPlayer.name = objDict["name"];

        if (objDict.ContainsKey("googleID"))
            newPlayer.googleID = objDict["googleID"];
        if (objDict.ContainsKey("token"))
            newPlayer.token = objDict["token"];

        return newPlayer;
    }

    public static bool ToBoolean(this string value)
    {
        switch (value.ToLower())
        {
            case "true":
                return true;
            case "t":
                return true;
            case "1":
                return true;
            case "0":
                return false;
            case "false":
                return false;
            case "f":
                return false;
            default:
                return false;
        }
    }
}
