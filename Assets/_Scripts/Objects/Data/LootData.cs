﻿using System.Collections;

public class LootData {

    public enum LootTypes
    {
        Matches = 1,
        Troop = 2,
        Box = 3,
        Currency = 4,
        Upgrade = 5
    }

    public int id, lootID, lootAmount, playerID;
    public string lootName;
    public LootTypes lootType;
}
