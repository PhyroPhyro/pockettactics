﻿using System;

[Serializable]
public class TroopData{

    public int PlayerID, BuyCost, UID, Life, MaxLife, TroopClass, TroopAbility, TurnsToSpawn, MaxSpawnTurns, Cost, Speed, ID, AtkDistance, AtkStr, SummonDistance, TypeID, TroopStatus;
    public bool CanFly = false;
    public bool CanMove = true;
    public bool CanAttack = true;
    public bool CanAttackFlying = true;
    public bool IsProtected = false;
    public bool HasDoubleAtk = false;
    public string Name;
}
