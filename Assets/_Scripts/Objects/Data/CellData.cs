﻿using System;

[Serializable]
public class CellData {

    public int id, cellTroopUID, value, xCoord, yCoord;
}
