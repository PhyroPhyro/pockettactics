﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TroopArmory : MonoBehaviour {

    public Text troopNameTxt;
    public TroopData troopData;

    public void Refresh(TroopData troop)
    {
        troopData = troop;
        troopNameTxt.text = troopData.Name;
    }

    public void OnClick()
    {
        ArmoryManager.instance.SelectArmoryTroop(troopData);
    }

    public void OnEnter()
    {
        FindObjectOfType<OverInfoPanelController>().SetInfoPanel(troopData);
    }

    public void OnExit()
    {
        FindObjectOfType<OverInfoPanelController>().HideInfoPanel();
    }
}
