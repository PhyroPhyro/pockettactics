﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player {

    public int id, globalRankExp, honorRankExp, nextHonorRank, nextGlobalRank;
    public string name, googleID, token, globalRank, honorRank;
    public List<TroopData> playerArmory = new List<TroopData>();
    public List<LootData> playerInventory = new List<LootData>();
}
