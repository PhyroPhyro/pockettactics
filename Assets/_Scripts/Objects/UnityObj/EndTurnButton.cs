﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class EndTurnButton : MonoBehaviour {

	public void OnClick()
    {
        ServerManager.ServerEndTurn(TurnEnded);
    }

    private void TurnEnded(Dictionary<string,string> objDict)
    {
        if (objDict.ContainsKey("message"))
        {
            if (objDict["message"] == "done")
            {
                Debug.Log("Turn ended and saved");
                MatchManager.instance.CurrentMatch = null;
                SceneManager.LoadScene("MainMenu");
            }
        }
    }
}
