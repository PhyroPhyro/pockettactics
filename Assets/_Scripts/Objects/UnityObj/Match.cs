﻿using UnityEngine;
using System.Collections;
using System;

public class Match {
    public int id, lastPlayed, player1ID, player2ID, turnID;
    public string opponentName;
    public DateTime lastUpdated;
    public MatchManager.MatchStatus status;
}
