﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TouchScript.Gestures;
using System;

public class Troop : MonoBehaviour {

    public int PlayerID, UID, Life, MaxLife, Cost, Speed, ID, AtkDistance, AtkStr, TurnsToSpawn, MaxSpawnTurns, SummonDistance;
    public string Name;
    public bool CanFly = false;
    public bool CanMove = true;
    public bool CanAttack = true;
    public bool CanAttackFlying = true;
    public bool IsProtected = false;
    public bool HasDoubleAtk = false;
    public TroopsManager.TroopsTypes TroopType = TroopsManager.TroopsTypes.Water;
    public TroopsManager.TroopsClasses TroopClass = TroopsManager.TroopsClasses.Leader;
    public TroopsManager.TroopStates TroopStatus = TroopsManager.TroopStates.Idle;
    public TroopsManager.TroopAbilities TroopAbility = TroopsManager.TroopAbilities.None;
    public GameObject TroopModel, LifeBar;
    public Material EnemyOutline, AllyOutline;

    private PressGesture pressGesture;
    private TapGesture tapGesture;
    private ReleaseGesture releaseGesture;

    void OnEnable()
    {
        pressGesture = GetComponent<PressGesture>();
        tapGesture = GetComponent<TapGesture>();
        releaseGesture = GetComponent<ReleaseGesture>();

        pressGesture.Pressed += pressHandler;
        tapGesture.Tapped += tapHandler;
        releaseGesture.Released += releaseHandler;
    }

    void OnDisable()
    {
        pressGesture.Pressed -= pressHandler;
        tapGesture.Tapped -= tapHandler;
        releaseGesture.Released -= releaseHandler;
    }

    private void pressHandler(object sender, EventArgs e)
    {
        FindObjectOfType<OverInfoPanelController>().SetInfoPanel(ToTroopData());
    }

    private void tapHandler(object sender, EventArgs e)
    {
        MapManager.instance.GetCellFromUID(UID).SelectCell();
    }

    private void releaseHandler(object sender, EventArgs e)
    {
        FindObjectOfType<OverInfoPanelController>().HideInfoPanel();
    }

    public IEnumerator MoveThisTroop(List<Cell> CellPath)
    {
        Vector3 lastPos = transform.position;
        float currentLerpTime = 0;
        float lerpTime = 0.7f;

        while (CellPath.Count > 0)
        {
            currentLerpTime += Time.deltaTime;
            if (currentLerpTime > lerpTime)
            {
                currentLerpTime = lerpTime;
            }
            float perc = currentLerpTime / lerpTime;
            transform.position = Vector3.Lerp(lastPos, CellPath[0].PositionNode.position, perc);
            if (transform.position == CellPath[0].PositionNode.position)
            {
                transform.position = CellPath[0].PositionNode.position;
                lastPos = transform.position;
                currentLerpTime = 0;
                CellPath.RemoveAt(0);
            }

            yield return null;
        }

        EventManager.TroopFinished();
    }

    public bool IsMine()
    {
        if (PlayerID == PlayerManager.instance.CurrentPlayer.id)
            return true;
        else
            return false;
    }

    public void IdleEvent()
    {

    }

    public void AttackingEvent()
    {
        EventManager.OnTroopAttacking -= AttackingEvent;
    }

    public void DefenseEvent()
    {
        EventManager.OnTroopDefending -= DefenseEvent;
    }

    public void DieEvent()
    {
        EventManager.OnTroopDie -= DieEvent;
        StartCoroutine(DieRoutine());
    }

    public void SkillEvent()
    {
        EventManager.OnTroopSkill -= SkillEvent;

    }

    IEnumerator DieRoutine()
    {
        //TODO - yield Apply Die animation
        yield return null;
        Destroy(gameObject);
    }

    public TroopData ToTroopData()
    {
        TroopData troopData = new TroopData();
        troopData.PlayerID = PlayerID;
        troopData.ID = ID;
        troopData.UID = UID;
        troopData.Name = Name;
        troopData.Life = Life;
        troopData.MaxLife = MaxLife;
        troopData.Cost = Cost;
        troopData.Speed = Speed;
        troopData.AtkDistance = AtkDistance;
        troopData.AtkStr = AtkStr;
        troopData.SummonDistance = SummonDistance;
        troopData.TypeID = (int)TroopType;
        troopData.TroopStatus = (int)TroopStatus;
        troopData.TroopClass = (int)TroopClass;
        troopData.TroopAbility = (int)TroopAbility;
        troopData.CanFly = CanFly;
        troopData.CanMove = CanMove;
        troopData.CanAttack = CanAttack;
        troopData.CanAttackFlying = CanAttack;
        troopData.IsProtected = IsProtected;
        troopData.HasDoubleAtk = HasDoubleAtk;
        return troopData;
    }

    public void FromTroopData(TroopData troopData)
    {
        PlayerID = troopData.PlayerID;
        ID = troopData.ID;
        UID = troopData.UID;
        Name = troopData.Name;
        Cost = troopData.Cost;
        Speed = troopData.Speed;
        MaxLife = troopData.MaxLife;
        Life = troopData.Life;
        AtkDistance = troopData.AtkDistance;
        AtkStr = troopData.AtkStr;
        SummonDistance = troopData.SummonDistance;
        TurnsToSpawn = troopData.TurnsToSpawn;
        MaxSpawnTurns = troopData.MaxSpawnTurns;
        TroopType = (TroopsManager.TroopsTypes)troopData.TypeID;
        TroopStatus = (TroopsManager.TroopStates)troopData.TroopStatus;
        TroopClass = (TroopsManager.TroopsClasses)troopData.TroopClass;
        TroopAbility = (TroopsManager.TroopAbilities)troopData.TroopAbility;
        CanFly = troopData.CanFly;
        CanMove = troopData.CanMove;
        CanAttack = troopData.CanAttack;
        CanAttackFlying = troopData.CanAttackFlying;
        IsProtected = troopData.IsProtected;
        HasDoubleAtk = troopData.HasDoubleAtk;

        StartCoroutine(UpdateGraphics());
    }

    IEnumerator UpdateGraphics()
    {
        Material[] troopMaterials = TroopModel.GetComponent<MeshRenderer>().materials;
        if (IsMine())
            troopMaterials[1] = AllyOutline;
        else
            troopMaterials[1] = EnemyOutline;
        TroopModel.GetComponent<MeshRenderer>().materials = troopMaterials;

        yield return null;

        float time = 0.7f;
        if(Life != MaxLife)
        {
            Vector3 originalScale = LifeBar.transform.localScale;
            Vector3 targetScale = new Vector3((float)Life/ (float)MaxLife, 1.0f, 1.0f);
            float originalTime = time;


            while (time > 0.0f)
            {
                time -= Time.deltaTime;
                LifeBar.transform.localScale = Vector3.Lerp(targetScale, originalScale, time / originalTime);
                yield return null;
            }
        }
    }
}
