﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TouchScript.Gestures;
using System;

public class Cell : MonoBehaviour {


    public MapManager.CellType CurrentType = MapManager.CellType.Empty;

    public int ID, xCoord, yCoord, CellTroopUID;
    public bool OnlyFlying = false;
    public List<Cell> NeighboursCells = new List<Cell>();
    public Color activeColor, inactiveColor, attackColor, summonColor;
    public List<Renderer> CellColorList = new List<Renderer>();
    public Transform PositionNode;

    private TapGesture tapGesture;

    private bool IsCellMovementActive = false;
    private bool IsCellAttackActive = false;
    private bool IsCellSummonActive = false;

    void OnEnable()
    {
        tapGesture = GetComponent<TapGesture>();

        tapGesture.Tapped += tapHandler;
    }

    void OnDisable()
    {
        tapGesture.Tapped -= tapHandler;
    }

    private void tapHandler(object sender, EventArgs e)
    {
        SelectCell();
    }

    public void SetCell(int _ID, MapManager.CellType _type)
    {
        ID = _ID;
        CurrentType = _type;

        if(CurrentType == MapManager.CellType.Empty)
        {
            Destroy(gameObject);
        }
    }

    public void SetNeighbours()
    {
        List<Cell> mapCells = MapManager.instance.MapCells;
        for (int i = 0; i < mapCells.Count; i++)
        {
            Transform currTransform = mapCells[i].transform;
            if (currTransform != transform && (currTransform.position - transform.position).sqrMagnitude < 3.5f)
                NeighboursCells.Add(mapCells[i]);
        }
    }

    public void SetCellActive(bool IsActive)
    {
        for (int i = 0; i < CellColorList.Count; i++)
        {
            if (IsActive)
            {
                CellColorList[i].material.color = activeColor;
            }
            else
            {
                CellColorList[i].material.color = inactiveColor;
            }
        }

        IsCellMovementActive = IsActive;
    }

    public void SetCellAttackActive(bool IsActive)
    {
        for (int i = 0; i < CellColorList.Count; i++)
        {
            if (IsActive)
            {
                CellColorList[i].material.color = attackColor;
            }
            else
            {
                CellColorList[i].material.color = inactiveColor;
            }
        }

        IsCellAttackActive = IsActive;
    }

    public void SetCellSummonActive(bool IsActive)
    {
        for (int i = 0; i < CellColorList.Count; i++)
        {
            if (IsActive)
            {
                CellColorList[i].material.color = summonColor;
            }
            else
            {
                CellColorList[i].material.color = inactiveColor;
            }
        }

        IsCellSummonActive = IsActive;
    }

    public void SelectCell()
    {
        Debug.Log(name + " was clicked");
        if (CellTroop() != null)
        {
            if (CellTroop().IsMine())
                MapManager.instance.SelectCellTroop(this);
            else if (IsCellAttackActive)
                TroopsManager.instance.AttackTroop(MapManager.instance.SelectedCell, this);
        }
        else if (IsCellMovementActive)
        {
            TroopsManager.instance.MoveTroop(MapManager.instance.SelectedCell, this);
        }
        else if (IsCellSummonActive)
        {
            TroopsManager.instance.SummonTroop(this);
        }
        else
        {
            MapManager.instance.UnselectAllCells();
        }
    }

    public Troop CellTroop()
    {
        return TroopsManager.instance.GetCellTroopByUID(CellTroopUID);
    }

    public CellData ToCellData()
    {
        CellData cellData = new CellData();
        cellData.id = ID;
        cellData.cellTroopUID = CellTroopUID;
        cellData.xCoord = xCoord;
        cellData.yCoord = yCoord;
        cellData.value = (int)CurrentType;

        return cellData;
    }

    public void FromCellData(CellData currCell)
    {
        ID = currCell.id;
        CellTroopUID = currCell.cellTroopUID;
        xCoord = currCell.xCoord;
        yCoord = currCell.yCoord;
        CurrentType = (MapManager.CellType)currCell.value;
    }
}
