﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class GameOverViewController : MonoBehaviour {

    public Text txResult, txStats;
    public GameObject contentObj;

    public void SetGameOver(Dictionary<string,string> objDict)
    {
        if(objDict["isWinner"] != "-1")
        {
            bool isWinner = StaticMethods.ToBoolean(objDict["isWinner"]);
            if (isWinner)
                txResult.text = "YOU WIN";
            else
                txResult.text = "YOU LOSE";
        }else
        {
            txResult.text = "DRAW";
        }

        txStats.text = string.Format("Your Global Rank EXP: {0}\nYour Honor Rank EXP: {1}\nYour Current Win Streak: {2}", objDict["globalRank"], objDict["honorRank"], objDict["winStreak"]);

        contentObj.SetActive(true);
    }

    public void OnConfirm()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
