﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PullLimitController : MonoBehaviour {

    public RectTransform upObj, downObj, leftObj, rightObj;
    public enum WindowSide
    {
        Up,
        Down,
        Left,
        Right
    }

    private float upCounter, downCounter, leftCounter, rightCounter;
    public float maxCounter, resizeSpeed, maxSize;

    public void ScaleObj(WindowSide side, float amount)
    {
        Vector3 amountScale = new Vector3(amount, amount, amount);
        switch(side)
        {
            case WindowSide.Up:
                if(upObj.localScale.x < maxSize)
                    upObj.localScale -= amountScale;
                upCounter = maxCounter;
                break;
            case WindowSide.Down:
                if (downObj.localScale.x < maxSize)
                    downObj.localScale -= amountScale;
                downCounter = maxCounter;
                break;
            case WindowSide.Left:
                if (leftObj.localScale.x < maxSize)
                    leftObj.localScale -= amountScale;
                leftCounter = maxCounter;
                break;
            case WindowSide.Right:
                if (rightObj.localScale.x < maxSize)
                    rightObj.localScale -= amountScale;
                rightCounter = maxCounter;
                break;
        }
    }

    void Update()
    {
        if (upCounter > 0)
            upCounter--;
        if (downCounter > 0)
            downCounter--;
        if (leftCounter > 0)
            leftCounter--;
        if (rightCounter > 0)
            rightCounter--;

        if (upCounter <= 0 && upObj.localScale.x > 1f)
            upObj.localScale -= new Vector3(resizeSpeed, resizeSpeed, resizeSpeed);
        else if (upObj.localScale.x < 1)
            upObj.localScale = Vector3.one;

        if (downCounter <= 0 && downObj.localScale.x > 1f)
            downObj.localScale -= new Vector3(resizeSpeed, resizeSpeed, resizeSpeed);
        else if (downObj.localScale.x < 1)
            downObj.localScale = Vector3.one;

        if (leftCounter <= 0 && leftObj.localScale.x > 1f)
            leftObj.localScale -= new Vector3(resizeSpeed, resizeSpeed, resizeSpeed);
        else if (leftObj.localScale.x < 1)
            leftObj.localScale = Vector3.one;

        if (rightCounter <= 0 && rightObj.localScale.x > 1f)
            rightObj.localScale -= new Vector3(resizeSpeed, resizeSpeed, resizeSpeed);
        else if (rightObj.localScale.x < 1)
            rightObj.localScale = Vector3.one;
    }
}
