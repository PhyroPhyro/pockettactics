﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SocketIO;
using System.Collections.Generic;

public class MatchesViewController : MonoBehaviour {

    public GameObject matchButtonObj, completedMatchObj, yourTurnContainer, theirTurnContainer, completedContainer;
    public Text txMatchesLeft;

    void OnEnable()
    {
        EventManager.OnPlayerInfoUpdate += RefreshMatchView;
    }

    void OnDisable()
    {
        EventManager.OnPlayerInfoUpdate -= RefreshMatchView;
    }

    void Start()
    {
        FindObjectOfType<SocketIOComponent>().On("UpdateClientMatches", SyncMatches);
        RefreshMatches();
    }

    public void RefreshMatchView()
    {
        List<LootData> playerInv = PlayerManager.instance.CurrentPlayer.playerInventory;
        for (int i = 0; i < playerInv.Count; i++)
        {
            if (playerInv[i].lootType == LootData.LootTypes.Matches)
            {
                txMatchesLeft.text = string.Format(playerInv[i].lootAmount > 1 ? "{0} Matches Left" : "{0} Match Left", playerInv[i].lootAmount);
                break;
            }
        }
    }

    public void RefreshMatches()
    {
        for (int i = 0; i < yourTurnContainer.transform.childCount; i++)
        {
            Destroy(yourTurnContainer.transform.GetChild(i).gameObject);
        }
        for (int i = 0; i < theirTurnContainer.transform.childCount; i++)
        {
            Destroy(theirTurnContainer.transform.GetChild(i).gameObject);
        }
        for (int i = 0; i < completedContainer.transform.childCount; i++)
        {
            Destroy(completedContainer.transform.GetChild(i).gameObject);
        }

        ServerManager.GetMyMatches();
    }

    public void SyncMatches(SocketIOEvent e)
    {
        Dictionary<string, string> objDict = e.data.ToDictionary();
        Match newMatch = MatchManager.MatchConverter(objDict);

        if(newMatch.status == MatchManager.MatchStatus.Completed)
        {
            GameObject newMatchObj = GameObject.Instantiate(completedMatchObj);
            newMatchObj.transform.SetParent(completedContainer.transform);
            newMatchObj.transform.localScale = Vector2.one;
            newMatchObj.GetComponent<CompletedMatchController>().SetMatchInfo(newMatch);

            if (newMatch.lastUpdated > completedContainer.transform.GetChild(0).GetComponent<MatchButtonController>().thisMatch.lastUpdated)
                newMatchObj.transform.SetAsFirstSibling();
        }
        else
        {
            GameObject currContainer = null;
            if (newMatch.lastPlayed == PlayerManager.instance.CurrentPlayer.id)
                currContainer = theirTurnContainer;
            else
                currContainer = yourTurnContainer;

            GameObject newMatchButton = GameObject.Instantiate(matchButtonObj);
            newMatchButton.transform.SetParent(currContainer.transform);
            newMatchButton.transform.localScale = Vector2.one;
            newMatchButton.GetComponent<MatchButtonController>().SetMatch(newMatch);

            if (newMatch.lastUpdated > currContainer.transform.GetChild(0).GetComponent<MatchButtonController>().thisMatch.lastUpdated)
                newMatchButton.transform.SetAsFirstSibling();
        }
    }

    public void CreateMatch()
    {
        MatchManager.instance.ChosenMap = 1;
        ServerManager.RequestNewMatch(1, CreateMatchCallback);
    }

    private void CreateMatchCallback(Dictionary<string, string> objDict)
    {
        Debug.Log("Match received!");
        Match newMatch = MatchManager.MatchConverter(objDict);
        MatchManager.instance.StartMatch(newMatch);
    }
}
