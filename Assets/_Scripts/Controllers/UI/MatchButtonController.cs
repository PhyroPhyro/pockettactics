﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class MatchButtonController : MonoBehaviour {

    public Match thisMatch;
    public Text btText;

    public void SetMatch(Match _match)
    {
        thisMatch = _match;
        TimeSpan timeAgo = DateTime.Now - thisMatch.lastUpdated;
        string timeAgoString = "";
        if (Mathf.Round((int)timeAgo.TotalDays) > 0)
            timeAgoString = string.Format(Mathf.Round((int)timeAgo.TotalDays).ToString() + " {0}", Mathf.Round((int)timeAgo.TotalDays) == 1 ? "day" : "days");
        else if (Mathf.Round((int)timeAgo.TotalHours) > 0)
            timeAgoString = string.Format(Mathf.Round((int)timeAgo.TotalHours).ToString() + " {0}", Mathf.Round((int)timeAgo.TotalHours) == 1 ? "hour" : "hours");
        else if (Mathf.Round((int)timeAgo.TotalMinutes) > 0)
            timeAgoString = string.Format(Mathf.Round((int)timeAgo.TotalMinutes).ToString() + " {0}", Mathf.Round((int)timeAgo.TotalMinutes) == 1 ? "minute" : "minutes");
        else
            timeAgoString = string.Format(Mathf.Round((int)timeAgo.TotalSeconds).ToString() + " {0}", Mathf.Round((int)timeAgo.TotalSeconds) == 1 ? "second" : "seconds");

        btText.text = string.Format("{0} vs {1}\n{2} ago", PlayerManager.instance.CurrentPlayer.name, thisMatch.opponentName != null ? thisMatch.opponentName : "???", timeAgoString);
    }

	public void OnClick()
    {
        if(thisMatch.lastPlayed != PlayerManager.instance.CurrentPlayer.id)
        {
            Debug.Log("Starting match " + thisMatch.id);
            MatchManager.instance.StartMatch(thisMatch);
        }
    }
}
