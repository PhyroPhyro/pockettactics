﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SocketIO;
using System.Collections.Generic;

public class ArmoryViewController : MonoBehaviour {

    public GameObject troopsContainer, armoryTroopObject, btSave;
    public ArmoryTroopController selectedLeaderTroop, selectedTankTroop, selectedMeleeTroop, selectedRangedTroop, selectedFlyingTroop, selectedBuildingTroop;
    private List<TroopData> allTroopsList = new List<TroopData>();
    private bool doneLoading = false;

    void OnEnable()
    {
        EventManager.OnPlayerInfoUpdate += PopulatePlayerArmory;
    }

    void OnDisable()
    {
        EventManager.OnPlayerInfoUpdate -= PopulatePlayerArmory;
    }

    IEnumerator Start()
    {
        FindObjectOfType<SocketIOComponent>().On("UpdateArmoryTroops", SyncArmoryTroops);
        ServerManager.GetArmoryTroops();

        while (doneLoading == false)
            yield return null;

    }

    private void SyncArmoryTroops(SocketIOEvent e)
    {
        Dictionary<string, string> objDict = e.data.ToDictionary();

        if (objDict.ContainsKey("message"))
        {
            if (objDict["message"] == "done")
            {
                doneLoading = true;
                Debug.Log("Armory troops done loading");
                return;
            }
        }
        
        TroopData currTroopData = StaticMethods.TroopDataFromDBDict(objDict);

        for (int i = 0; i < allTroopsList.Count; i++)
        {
            if (allTroopsList[i].ID == currTroopData.ID)
            {
                allTroopsList[i] = currTroopData;
                return;
            }
        }

        allTroopsList.Add(currTroopData);

        PopulateArmory(TroopsManager.TroopsClasses.Leader);
    }

    public void PopulateArmory(TroopsManager.TroopsClasses selectedClass)
    {
        for (int i = 0; i < troopsContainer.transform.childCount; i++)
        {
            Destroy(troopsContainer.transform.GetChild(i).gameObject);
        }

        for (int i = 0; i < allTroopsList.Count; i++)
        {
            if(allTroopsList[i].TroopClass == (int)selectedClass)
            {
                GameObject newObj = Instantiate(armoryTroopObject);
                newObj.transform.localScale = Vector3.one;
                newObj.transform.SetParent(troopsContainer.transform);
                newObj.GetComponent<ArmoryTroopController>().SetArmoryTroop(allTroopsList[i]);
            }
        }
    }

    public void PopulatePlayerArmory()
    {
        List<TroopData> currPlayerArmory = PlayerManager.instance.CurrentPlayer.playerArmory;

        currPlayerArmory = PlayerManager.instance.CurrentPlayer.playerArmory;

        for (int i = 0; i < currPlayerArmory.Count; i++)
        {
            SetTroopToArmory(currPlayerArmory[i]);
        }

        PopulateArmory(TroopsManager.TroopsClasses.Leader);
    }

    public bool ArmoryHasTroop(TroopData _troopData)
    {
        if (selectedLeaderTroop.thisTroop.ID == _troopData.ID || selectedTankTroop.thisTroop.ID == _troopData.ID ||
            selectedMeleeTroop.thisTroop.ID == _troopData.ID || selectedRangedTroop.thisTroop.ID == _troopData.ID ||
            selectedFlyingTroop.thisTroop.ID == _troopData.ID || selectedBuildingTroop.thisTroop.ID == _troopData.ID)
            return true;
        else
            return false;
    }

    public void SetTroopToArmory(TroopData _troopData, bool isChanging = false)
    {
        switch (_troopData.TroopClass)
        {
            case (int)TroopsManager.TroopsClasses.Leader:
                selectedLeaderTroop.SetArmoryTroop(_troopData);
                break;
            case (int)TroopsManager.TroopsClasses.Tank:
                selectedTankTroop.SetArmoryTroop(_troopData);
                break;
            case (int)TroopsManager.TroopsClasses.Melee:
                selectedMeleeTroop.SetArmoryTroop(_troopData);
                break;
            case (int)TroopsManager.TroopsClasses.Ranged:
                selectedRangedTroop.SetArmoryTroop(_troopData);
                break;
            case (int)TroopsManager.TroopsClasses.Flying:
                selectedFlyingTroop.SetArmoryTroop(_troopData);
                break;
            case (int)TroopsManager.TroopsClasses.Building:
                selectedBuildingTroop.SetArmoryTroop(_troopData);
                break;
        }

        PopulateArmory((TroopsManager.TroopsClasses)_troopData.TroopClass);

        if (isChanging)
            btSave.SetActive(true);
    }

    public void SaveArmory()
    {
        PopupManager.instance.SetPopup(PopupManager.PopupType.ConfirmationPopup, "Confirm", "Do you want to save this armory selection?", null, SendArmory);
    }

    private void SendArmory()
    {
        ServerManager.SavePlayerArmory(selectedLeaderTroop.thisTroop.ID,
                                       selectedTankTroop.thisTroop.ID,
                                       selectedMeleeTroop.thisTroop.ID,
                                       selectedRangedTroop.thisTroop.ID,
                                       selectedFlyingTroop.thisTroop.ID,
                                       selectedBuildingTroop.thisTroop.ID,
                                       WaitForArmoryConfirmation);
    }

    private void WaitForArmoryConfirmation(Dictionary<string, string> objDict)
    {
        if (objDict.ContainsKey("message"))
        {
            if (objDict["message"] == "ok")
            {
                //All ok on armory save
                PopupManager.instance.SetPopup(PopupManager.PopupType.InfoPopup, "Saved", "Armory saved");
                PlayerManager.instance.RefreshPlayerArmory();
                btSave.SetActive(false);
            }
            else
            {
                Debug.Log(objDict["message"]);
                PopupManager.instance.SetPopup(PopupManager.PopupType.InfoPopup, "Error", objDict["message"]);
            }
        }
    }

    public void BuyTroop(TroopData _thisTroop)
    {
        ServerManager.BuyTroop(_thisTroop.ID, WaitForBuyConfirmation);
    }

    private void WaitForBuyConfirmation(Dictionary<string, string> objDict)
    {
        if (objDict.ContainsKey("message"))
        {
            PlayerManager.instance.RefreshPlayerInventory();
            PopupManager.instance.SetPopup(PopupManager.PopupType.InfoPopup, "Success", objDict["message"]);
        }
        else if (objDict.ContainsKey("error"))
        {
            PopupManager.instance.SetPopup(PopupManager.PopupType.InfoPopup, "Error", objDict["error"]);
        }
    }
}