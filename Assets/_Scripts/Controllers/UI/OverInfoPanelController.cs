﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OverInfoPanelController : MonoBehaviour {

    public GameObject contentObj;
    public Text txName, txType, txHP, txClass, txCost, txSpeed, txDistance, txStr, txAbility, txAtkFly, txFlyGround;

    public void SetInfoPanel(TroopData troopInfo)
    {
        txName.text = troopInfo.Name;
        txType.text = ((TroopsManager.TroopsTypes)troopInfo.TypeID).ToString();
        txHP.text = troopInfo.MaxLife > 0 ? string.Format("{0}/{1}", troopInfo.Life, troopInfo.MaxLife) : troopInfo.Life.ToString();
        txClass.text = ((TroopsManager.TroopsClasses)troopInfo.TroopClass).ToString();
        txCost.text = troopInfo.Cost.ToString();
        txSpeed.text = troopInfo.Speed.ToString();
        txDistance.text = troopInfo.AtkDistance.ToString();
        txStr.text = troopInfo.AtkStr.ToString();
        txAbility.text = ((TroopsManager.TroopAbilities)troopInfo.TroopAbility).ToString();
        txAtkFly.text = troopInfo.CanAttackFlying ? "Atk Flying/Ground" : "Atk Ground Only";
        txFlyGround.text = troopInfo.CanFly ? "Flying" : "Grounded";

        contentObj.SetActive(true);
    }

    public void HideInfoPanel()
    {
        contentObj.SetActive(false);
    }

    void Update()
    {
        if (contentObj.activeInHierarchy)
            contentObj.transform.position = Input.mousePosition;
    }
}
