﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class MainMenuViewController : MonoBehaviour {

    public Text txWelcome, txCurrentGlobalRank, txCurrentHonorRank, txSignalType, txCurrency;
    public Scrollbar middleScrollBar;
    public GameObject middleScrollContainer;

    void Awake()
    {
        PlayerManager.instance.RefreshPlayerInventory();
    }

    void OnEnable()
    {
        EventManager.OnPlayerInfoUpdate += RefreshMainView;
    }

    void OnDisable()
    {
        EventManager.OnPlayerInfoUpdate -= RefreshMainView;
    }

    void Start()
    {
        middleScrollBar.value = 0.5f;
        txWelcome.text = string.Format("Welcome {0}", PlayerManager.instance.CurrentPlayer.name);

        string currentNetwork = "";
        if (Application.internetReachability == NetworkReachability.ReachableViaCarrierDataNetwork)
            currentNetwork = "Mobile Network";
        else if (Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork)
            currentNetwork = "Wi-fi Network";
        txSignalType.text = currentNetwork;

        ServerManager.GetMyRanks();
    }

    public void RefreshMainView()
    {
        Player currentPlayer = PlayerManager.instance.CurrentPlayer;

        List<LootData> playerInv = new List<LootData>();
        playerInv = currentPlayer.playerInventory;

        for (int i = 0; i < playerInv.Count; i++)
        {
            if (playerInv[i].lootType == LootData.LootTypes.Currency)
                txCurrency.text = string.Format("Currency\n{0}", playerInv[i].lootAmount);
        }

        txCurrentGlobalRank.text = string.Format("Your Global Rank is {0} - {1}/{2}", currentPlayer.globalRank, currentPlayer.globalRankExp, currentPlayer.nextGlobalRank);
        txCurrentHonorRank.text = string.Format("You're a {0} - {1}/{2}", currentPlayer.honorRank, currentPlayer.honorRankExp, currentPlayer.nextHonorRank);
    }

    public void SendToPage(int page)
    {
        StartCoroutine(MiddleScrollToPage(page));
    }

    IEnumerator MiddleScrollToPage(float page)
    {
        float totalPages = middleScrollContainer.transform.childCount - 1;
        float allPagesValue = 1 / totalPages;

        float time = 0.3f;
        float originalValue = middleScrollBar.value;
        float targetValue = allPagesValue * page;
        float originalTime = time;

        while (time > 0.0f)
        {
            time -= Time.deltaTime;
            middleScrollBar.value = Mathf.Lerp(targetValue, originalValue, time / originalTime);
            yield return null;
        }
    }
}
