﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ArmoryTroopController : MonoBehaviour {

    public TroopData thisTroop;
    public Text txName;
    private ArmoryViewController viewController;
    private bool HasTroop = false;

    public void SetArmoryTroop(TroopData _troopData)
    {
        thisTroop = _troopData;
        RefreshVisuals();
    }

    public void RefreshVisuals()
    {
        viewController = FindObjectOfType<ArmoryViewController>();

        //Refresh visuals based on thisTroop and if it is on players armory selected already

        List<LootData> playerInv = PlayerManager.instance.CurrentPlayer.playerInventory;
        for (int i = 0; i < playerInv.Count; i++)
        {
            if (playerInv[i].lootType == LootData.LootTypes.Troop && playerInv[i].lootID == thisTroop.ID)
            {
                HasTroop = true;
                GetComponent<Image>().color = Color.white;
                txName.text = thisTroop.Name;
            }
        }

        if(!HasTroop)
            txName.text = thisTroop.Name + "\n" + thisTroop.BuyCost + " Credits";

        if (viewController.ArmoryHasTroop(thisTroop))
            GetComponent<Image>().color = Color.yellow;
    }

    public void SetTroopToArmory()
    {
        viewController.SetTroopToArmory(thisTroop, true);
        RefreshVisuals();
    }

    public void OnClick()
    {
        if(HasTroop)
        {
            if (!viewController.ArmoryHasTroop(thisTroop))
            {
                SetTroopToArmory();
            }else
            {
                viewController.PopulateArmory((TroopsManager.TroopsClasses)thisTroop.TroopClass);
            }
        }else
        {
            PopupManager.instance.SetPopup(PopupManager.PopupType.ConfirmationPopup, "Confirmation", "Do you really wants to use credits to buy this troop?", null, BuyThisTroop);
        }
    }

    private void BuyThisTroop()
    {
        viewController.BuyTroop(thisTroop);
    }

    public void OnEnter()
    {
        FindObjectOfType<OverInfoPanelController>().SetInfoPanel(thisTroop);
    }

    public void OnExit()
    {
        FindObjectOfType<OverInfoPanelController>().HideInfoPanel();
    }
}
