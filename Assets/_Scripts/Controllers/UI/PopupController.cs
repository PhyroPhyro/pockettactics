﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PopupController : MonoBehaviour {

    public Text titleText, bodyText, subText;

    public void OnConfirm()
    {
        PopupManager.instance.OnConfirmation();
    }
}
