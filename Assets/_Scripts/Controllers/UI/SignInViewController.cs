﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SignInViewController : MonoBehaviour {

    public GameObject nicknameInputBox;
    public Text nicknameInput;

    public void ShowNicknameInput()
    {
        nicknameInputBox.SetActive(true);
    }

    public void SubmitNicknameInput()
    {
        nicknameInputBox.SetActive(false);
        SignInManager.instance.SubmitAccountNickname(nicknameInput.text);
    }
}
