﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CompletedMatchController : MonoBehaviour {

    public Text MatchText;

    public void SetMatchInfo(Match _match)
    {
        MatchText.text = string.Format("{0} vs {1}\n{2}", PlayerManager.instance.CurrentPlayer.name, _match.opponentName, _match.lastUpdated.ToLocalTime().ToString());
    }
}
