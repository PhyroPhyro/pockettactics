﻿/*
 * @author Valentin Simonov / http://va.lent.in/
 */

using UnityEngine;
using TouchScript.Gestures;

namespace TouchScript.Examples.CameraControl
{
    public class CameraController : MonoBehaviour
    {
        public ScreenTransformGesture TwoFingerMoveGesture;
        public ScreenTransformGesture ManipulationGesture;
        public float PanSpeed = 200f;
        public float RotationSpeed = 200f;
        public float ZoomSpeed = 10f;
        public float maxPanX, minPanX, maxPanY, minPanY, maxZoom, minZoom;

        public Transform pivot;
        public Transform cam;

        private void OnEnable()
        {
            TwoFingerMoveGesture.Transformed += twoFingerTransformHandler;
            ManipulationGesture.Transformed += manipulationTransformedHandler;
        }

        private void OnDisable()
        {
            TwoFingerMoveGesture.Transformed -= twoFingerTransformHandler;
            ManipulationGesture.Transformed -= manipulationTransformedHandler;
        }

        private void manipulationTransformedHandler(object sender, System.EventArgs e)
        {
            Debug.Log(ManipulationGesture.DeltaScale * ZoomSpeed);
            if(ManipulationGesture.DeltaScale > 1)
            {
                cam.GetComponent<Camera>().orthographicSize -= ManipulationGesture.DeltaScale * ZoomSpeed;
                if (cam.GetComponent<Camera>().orthographicSize < minZoom)
                    cam.GetComponent<Camera>().orthographicSize += ManipulationGesture.DeltaScale * ZoomSpeed;
            }
            else
            {
                cam.GetComponent<Camera>().orthographicSize += ManipulationGesture.DeltaScale * ZoomSpeed;
                if (cam.GetComponent<Camera>().orthographicSize > maxZoom)
                    cam.GetComponent<Camera>().orthographicSize -= ManipulationGesture.DeltaScale * ZoomSpeed;
            }
        }

        private void twoFingerTransformHandler(object sender, System.EventArgs e)
        {
            pivot.localPosition += pivot.localRotation *TwoFingerMoveGesture.DeltaPosition * PanSpeed;

            if (pivot.localPosition.x > maxPanX)
                FindObjectOfType<PullLimitController>().ScaleObj(PullLimitController.WindowSide.Right, PanSpeed);
            if (pivot.localPosition.x < minPanX)
                FindObjectOfType<PullLimitController>().ScaleObj(PullLimitController.WindowSide.Left, PanSpeed);
            if (pivot.localPosition.y > maxPanY)
                FindObjectOfType<PullLimitController>().ScaleObj(PullLimitController.WindowSide.Up, PanSpeed);
            if (pivot.localPosition.y < minPanY)
                FindObjectOfType<PullLimitController>().ScaleObj(PullLimitController.WindowSide.Down, PanSpeed);

            if (pivot.localPosition.x > maxPanX || pivot.localPosition.x < minPanX || pivot.localPosition.y < minPanY || pivot.localPosition.y > maxPanY)
                pivot.localPosition -= pivot.localRotation * TwoFingerMoveGesture.DeltaPosition * PanSpeed;
        }
    }
}